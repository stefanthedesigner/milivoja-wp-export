<?php

/*
Plugin Name: BM Shortcode
Plugin URI: http://www.uiueux.com/
Description: BM Shortcode
Author: SeaTheme
Version: 1.0
Text Domain: bm-shortcodes 
Author URI: http://www.uiueux.com
*/

define('UX_THEME_SHORTCODES', untrailingslashit( plugins_url( '', __FILE__ ) ) );

//Main class
class UxShortcodes {

    function __construct() {
    	require_once( 'theme-shortcodes.php' );
    	define('UX_TINYMCE_URI', untrailingslashit( plugins_url( '', __FILE__ ) ). '/tinymce');
		define('UX_TINYMCE_DIR', untrailingslashit( dirname( __FILE__ ) ).'/tinymce');
		define('UX_SHORTCODE_URI', untrailingslashit( plugins_url( '', __FILE__ ) ). '/');

        add_action('init', array(&$this, 'init'));
	}

	function init() {
		add_action('wp_enqueue_scripts', array( &$this, 'ux_sc_enqueue_init'), 100);

		if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )
			return;

	}


	function ux_sc_enqueue_init() {
		
		//global $post;
		
		//css
		wp_enqueue_style( 'ux-interface-shortcode-css', UX_SHORTCODE_URI . 'css/bm-shortcode.css', false, '1.0', 'all' );
		
		//js
		wp_enqueue_script( 'jquery-collageplus', UX_SHORTCODE_URI . 'js/jquery.collageplus.min.js', array( 'jquery' ), '0.3.3', true);
		wp_enqueue_script( 'ux-shortcode-js', UX_SHORTCODE_URI . 'js/bm-shortcode.js', array( 'jquery' ), '1.0.0', true);

	}

}
$ux_shortcodes_obj = new UxShortcodes();
?>