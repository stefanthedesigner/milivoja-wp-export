<?php
/**
 * Plugin Name: Codeless Framework
 * Plugin URI: http://codeless.co
 * Description: Codeless Framework Tools
 * Version: 1.0.0
 * Author: Codeless
 * Author URI: http://codeless.co
 * License: GPL2
 */
 
 // don't load directly
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

function codeless_widget_register( $widget ){
	register_widget( $widget );
}


function cl_remove_wpautop( $content, $autop = false ) {

	if ( $autop ) {
		$content = wpautop( preg_replace( '/<\/?p\>/', "\n", $content ) . "\n" );
	}

	return do_shortcode( shortcode_unautop( $content ) );
}


function codeless_shortcode_add( $tag, $func ){
	add_shortcode($tag, $func);
}


/*
 * Inserts a new key/value before the key in the array.
 *
 * @param $key
 *   The key to insert before.
 * @param $array
 *   An array to insert in to.
 * @param $new_key
 *   The key to insert.
 * @param $new_value
 *   An value to insert.
 *
 * @return
 *   The new array if the key exists, FALSE otherwise.
 *
 * @see array_insert_after()
 */
function array_insert_before($key, array &$array, $new_key, $new_value) {
  if (array_key_exists($key, $array)) {
    $new = array();
    foreach ($array as $k => $value) {
      if ($k === $key) {
        $new[$new_key] = $new_value;
      }
      $new[$k] = $value;
    }
    return $new;
  }
  return FALSE;
}


function cl_remove_empty_p( $content ){
	
	if( is_customize_preview() )
		return $content;

	if( substr_count($content, '<p') == 1 && substr_count($content, '<p>') == 1 )
		$content = str_replace( array('<p>', '</p>'), array('', ''), $content );
	else
		$content = str_replace( array('<p', '/p>'), array('<span', '/span>'), $content );

	$content = str_replace( array('<div', '/div>'), array('<span', '/span>'), $content );

	return $content;
}


function codeless_decode_content($data){
	return base64_decode( $data );
}

function codeless_encode_content($data){
	return base64_encode( $data );
}

function codeless_builder_generic_read_file( $file ){
	$content = "";
    
    if( ! function_exists('codeless_decode_content') )
        return false;

    if ( file_exists($file) ) {
                
        $content = codeless_generic_get_content($file);

        if ($content) {

            if( ! empty( $content ) ){
                $decoded_content = codeless_decode_content($content);

                if( !empty( $decoded_content ) )
                    $unserialized_content = unserialize( $decoded_content );

                if ($unserialized_content) {
                    return $unserialized_content;
                }
            }else{
                return '';
            }
        }
        return false;
    }
}

function codeless_builder_generic_get_content($file){
	$content = '';
    if ( function_exists('realpath') )
        $filepath = realpath($file);

    if ( !$filepath || !@is_file($filepath) )
        return '';

    if( ini_get('allow_url_fopen') ) {
        $method = 'fopen';
    } else {
        $method = 'file_get_contents';
    }
    
    if ( $method == 'fopen' ) {
        $handle = fopen( $filepath, 'rw' );

        if( $handle !== false ) {
            if( filesize( $filepath ) > 0 ){
                while (!feof($handle)) {
                    $content .= fread($handle, filesize( $filepath ) );
                }
                fclose( $handle );
            }
        }
        return $content;
    } else {
        return file_get_contents($filepath);
    }
}

function codeless_builder_file_open( $filename, $mode ){
	return fopen( $filename, $mode );
}

function codeless_builder_file_close( $fp ){
	return fclose( $fp );
}

function codeless_builder_f_get_contents( $data ){
	return file_get_contents($data);
}

function codeless_http_user_agent(){
	return isset( $_SERVER['HTTP_USER_AGENT'] ) ? $_SERVER['HTTP_USER_AGENT'] : '';
}

function codeless_isLocalhost(){
	return ( $_SERVER['REMOTE_ADDR'] === '127.0.0.1' || $_SERVER['REMOTE_ADDR'] === 'localhost' || $_SERVER['REMOTE_ADDR'] === '::1') ? 1 : 0;
}

function codeless_server_software(){
	return $_SERVER['SERVER_SOFTWARE'];
}

function codeless_request_uri(){
	return isset( $_SERVER['REQUEST_URI'] ) ? $_SERVER['REQUEST_URI'] : '';
}


function codeless_add_submenu_page($a1, $a2, $a3, $a4, $a5, $a6){

	add_submenu_page( $a1, $a2, $a3, $a4, $a5, $a6 );
}

function codeless_add_menu_page($a1, $a2, $a3, $a4, $a5, $a6, $a7){
	add_menu_page( $a1, $a2, $a3, $a4, $a5, $a6, $a7 );
}


if( !function_exists ( 'codeless_widget_register' ) ){
	function codeless_widget_register( $param1 ){
		register_widget( $param1 );
	}
}

 ?>