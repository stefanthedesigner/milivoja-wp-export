<?php
//count down template
function ux_pb_module_countdown($itemid){
	global $ux_pagebuilder; 
	$module_post = $ux_pagebuilder->item_postid($itemid);
	$moduleid = 'text-block';
	
	if($module_post){
		//count down confing
		$time              = get_post_meta($module_post, 'module_countdown_time', true);
		$start             = get_post_meta($module_post, 'module_countdown_start', true);
		$end               = get_post_meta($module_post, 'module_countdown_end', true);
		
		$advanced_settings = get_post_meta($module_post, 'module_advanced_settings', true);
		$animation_base    = get_post_meta($module_post, 'module_scroll_animation_base', true);
		
		$animation_style   = $advanced_settings == 'on' ? ux_pb_module_animation_style($itemid, $moduleid) : false;
		$animation_end     = $advanced_settings == 'on' ? ux_pb_animation_end($animation_base) : false;
		
		$date_array = array(
			'years' => 0,
			'months' => 1,
			'days' => 2,
			'hours' => 3,
			'minutes' => 4,
			'seconds' => 5
		);
		
		$start = $start ? $date_array[$start] : $date_array['years'];
		$end = $end ? $date_array[$end] : $date_array['seconds'];
		
		$date_format = false;
		foreach($date_array as $date => $i){
			if($i >= $start && $i <= $end){
				switch($date){
					case 'years': $date_format .= 'y'; break;
					case 'months': $date_format .= 'o'; break;
					case 'days': $date_format .= 'd'; break;
					case 'hours': $date_format .= 'H'; break;
					case 'minutes': $date_format .= 'M'; break;
					case 'seconds': $date_format .= 'S'; break;
				}
			}
		}
		
		$date = new DateTime($time); ?>
        <div class="countdown <?php echo esc_attr($animation_style); ?>" data-animationend="<?php echo esc_attr($animation_end); ?>" data-years="<?php echo esc_attr($date->format('Y')); ?>" data-months="<?php echo esc_attr($date->format('n')); ?>" data-days="<?php echo esc_attr($date->format('d')); ?>" data-hours="<?php echo esc_attr($date->format('H')); ?>" data-minutes="<?php echo esc_attr($date->format('i')); ?>" data-seconds="<?php echo esc_attr($date->format('s')); ?>" data-dateformat="<?php echo esc_attr($date_format); ?>"></div>
	<?php
	}
}
add_action('ux-pb-module-template-count-down', 'ux_pb_module_countdown');

//count down select fields
function ux_pb_module_countdown_select($fields){
	$fields['module_countdown_start'] = array(
		array('title' => __('Years','bm-pagebuilder'), 'value' => 'years'),
		array('title' => __('Months','bm-pagebuilder'), 'value' => 'months'),
		array('title' => __('Days','bm-pagebuilder'), 'value' => 'days'),
		array('title' => __('Hours','bm-pagebuilder'), 'value' => 'hours'),
		array('title' => __('Minutes','bm-pagebuilder'), 'value' => 'minutes'),
		array('title' => __('Seconds','bm-pagebuilder'), 'value' => 'seconds')
	);
	
	$fields['module_countdown_end'] = array(
		array('title' => __('Years','bm-pagebuilder'), 'value' => 'years'),
		array('title' => __('Months','bm-pagebuilder'), 'value' => 'months'),
		array('title' => __('Days','bm-pagebuilder'), 'value' => 'days'),
		array('title' => __('Hours','bm-pagebuilder'), 'value' => 'hours'),
		array('title' => __('Minutes','bm-pagebuilder'), 'value' => 'minutes'),
		array('title' => __('Seconds','bm-pagebuilder'), 'value' => 'seconds')
	);
	
	return $fields;
}
add_filter('ux_pb_module_select_fields', 'ux_pb_module_countdown_select');

//count down config fields
function ux_pb_module_countdown_fields($module_fields){
	$module_fields['count-down'] = array(
		'id' => 'count-down',
		'animation' => true,
		'title' => __('Count Down','bm-pagebuilder'),
		'item' =>  array(
			array('title' => __('Date','bm-pagebuilder'),
				  'description' => __('Select a deadline for the counter','bm-pagebuilder'),
				  'type' => 'date',
				  'name' => 'module_countdown_time'),
				  
			array('title' => __('Count Start','bm-pagebuilder'),
				  'description' => __('Choose a start time unit','bm-pagebuilder'),
				  'type' => 'select',
				  'name' => 'module_countdown_start',
				  'default' => 'years'),
				  
			array('title' => __('Count To','bm-pagebuilder'),
				  'description' => __('Choose a end time unit','bm-pagebuilder'),
				  'type' => 'select',
				  'name' => 'module_countdown_end',
				  'default' => 'seconds'),
				  
			array('title' => __('Advanced Settings','bm-pagebuilder'),
				  'description' => __('magin and animations','bm-pagebuilder'),
				  'type' => 'switch',
				  'name' => 'module_advanced_settings',
				  'default' => 'off'),
				  
			array('title' => __('Bottom Margin','bm-pagebuilder'),
				  'description' => __('the spacing outside the bottom of module','bm-pagebuilder'),
				  'type' => 'select',
				  'name' => 'module_bottom_margin',
				  'default' => 'bottom-space-40',
				  'control' => array(
					  'name' => 'module_advanced_settings',
					  'value' => 'on'
				  ))
		)
	);
	return $module_fields;
	
}
add_filter('ux_pb_module_fields', 'ux_pb_module_countdown_fields');
?>