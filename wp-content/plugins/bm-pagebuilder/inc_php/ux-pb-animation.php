<?php
//animation lib
function ux_pb_animation_base($fields){
	$fields['module_scroll_animation_base'] = array(
		array('title' => __('Fade In','bm-pagebuilder'),             'value' => 'fadein'),
		array('title' => __('Zoom In','bm-pagebuilder'),             'value' => 'zoomin'),
		array('title' => __('Zoom Out','bm-pagebuilder'),            'value' => 'zoomout'),
		array('title' => __('Fade In Left','bm-pagebuilder'),        'value' => 'from-left-translate'),
		array('title' => __('Fade In Right','bm-pagebuilder'),       'value' => 'from-right-translate'),
		array('title' => __('Fade In Top','bm-pagebuilder'),         'value' => 'from-top-translate'),
		array('title' => __('Fade In Bottom','bm-pagebuilder'),      'value' => 'from-bottom-translate'),
		array('title' => __('Bounce In Left','bm-pagebuilder'),      'value' => 'bouncdein-left-translate'),
		array('title' => __('Bounce In Right','bm-pagebuilder'),     'value' => 'bouncdein-right-translate'),
		array('title' => __('Bounce In Top','bm-pagebuilder'),       'value' => 'bouncdein-top-translate'),
		array('title' => __('Bounce In Bottom','bm-pagebuilder'),    'value' => 'bouncdein-bottom-translate'),
		array('title' => __('Flip X','bm-pagebuilder'),              'value' => 'flip-x-translate'),
		array('title' => __('Flip Y','bm-pagebuilder'),              'value' => 'flip-y-translate'),
		array('title' => __('Rotate In DownLeft','bm-pagebuilder'),  'value' => 'rotate-downleft-translate'),
		array('title' => __('Rotate In DownRight','bm-pagebuilder'), 'value' => 'rotate-downright-translate')
	);
	
	return $fields;
}
add_filter('ux_pb_module_select_fields', 'ux_pb_animation_base');

function ux_pb_animation_end($anima){
	$return = 'fadeined';
	switch($anima){
		case 'fadein':                     $return = 'fadeined'; break;
		case 'zoomin':                     $return = 'zoomined'; break;
		case 'zoomout':                    $return = 'zoomouted'; break;
		case 'from-left-translate':        $return = 'from-left-translated'; break;
		case 'from-right-translate':       $return = 'from-right-translated'; break;
		case 'from-top-translate':         $return = 'from-top-translated'; break;
		case 'from-bottom-translate':      $return = 'from-bottom-translated'; break;
		case 'bouncdein-left-translate':   $return = 'bouncdein-left-translated'; break;
		case 'bouncdein-right-translate':  $return = 'bouncdein-right-translated'; break;
		case 'bouncdein-top-translate':    $return = 'bouncdein-top-translated'; break;
		case 'bouncdein-bottom-translate': $return = 'bouncdein-bottom-translated'; break;
		case 'flip-x-translate':           $return = 'flip-x-translated'; break;
		case 'flip-y-translate':           $return = 'flip-y-translated'; break;
		case 'rotate-downleft-translate':  $return = 'rotate-downleft-translated'; break;
		case 'rotate-downright-translate': $return = 'rotate-downright-translated'; break;
	}
	return $return;
}

function ux_pb_animation($module_fields){
	$animation = array(
		array('title' => __('Scroll in Animation','bm-pagebuilder'),
			  'description' => __('enable to select Scroll in animation effect','bm-pagebuilder'),
			  'type' => 'switch',
			  'name' => 'module_scroll_in_animation',
			  'default' => 'off',
			  'control' => array(
				  'name' => 'module_advanced_settings',
				  'value' => 'on'
			  )),
			  
		array('title' => __('Scroll in Animation Effect','bm-pagebuilder'),
			  'description' => __('animation effect when the module enter the scene','bm-pagebuilder'),
			  'type' => 'select',
			  'name' => 'module_scroll_animation_base',
			  'default' => 'fadein',
			  'control' => array(
				  'name' => 'module_scroll_in_animation',
				  'value' => 'on'
			  ))
	);
	
	if($module_fields){
		foreach($module_fields as $moduleid => $fields){
			if(isset($fields['animation'])){
				if($fields['animation']){
					foreach($animation as $anima){
						if($moduleid == 'icon-box'){
							$anima['modal-body'] = 'after';
						}
						array_push($module_fields[$moduleid]['item'], $anima);
					}
				}
			}
			
		}
	}
	
	return $module_fields;
}

?>