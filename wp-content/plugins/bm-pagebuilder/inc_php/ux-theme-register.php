<?php
function ux_theme_register_other_post_type($register){
	$register['team_item'] = array(
		'name' => __('Team','bm-pagebuilder'),
		'meta' => true,
		'add_new' => __('Add New','bm-pagebuilder'),
		'add_new_item' => __('Add New Team Member','bm-pagebuilder'),
		'edit_item' => __('Edit Team Member','bm-pagebuilder'),
		'new_item' => __('New Team Member','bm-pagebuilder'),
		'view_item' => __('View Team Member','bm-pagebuilder'),
		'not_found' => __('No Team Member found.','bm-pagebuilder'),
		'not_found_in_trash' => __('No Team Member found in Trash.','bm-pagebuilder'),
		'search_items' => __('Search Team Member','bm-pagebuilder'),
		'cat_slug' => __('team_cat','bm-pagebuilder'),
		'cat_menu_name' => __('Team Categories','bm-pagebuilder'),
		'columns' => array(
			'column_category' => __('Categories','bm-pagebuilder')
		),
		'menu_icon' => UX_PAGEBUILDER. '/images/icon/post-type/team.png'
	
	);
	
	$register['clients_item'] = array(
		'name' => __('Clients','bm-pagebuilder'),
		'meta' => true,
		'add_new' => __('Add New','bm-pagebuilder'),
		'add_new_item' => __('Add New Client','bm-pagebuilder'),
		'edit_item' => __('Edit Client','bm-pagebuilder'),
		'new_item' => __('New Client','bm-pagebuilder'),
		'view_item' => __('View Client','bm-pagebuilder'),
		'not_found' => __('No Client found.','bm-pagebuilder'),
		'not_found_in_trash' => __('No Client found in Trash.','bm-pagebuilder'),
		'search_items' => __('Search Client','bm-pagebuilder'),
		'cat_slug' => __('client_cat','bm-pagebuilder'),
		'cat_menu_name' => __('Client Categories','bm-pagebuilder'),
		'columns' => array(
			'column_category' => __('Categories','bm-pagebuilder')
		),
		'menu_icon' => UX_PAGEBUILDER. '/images/icon/post-type/client.png',
		'remove_support' => array('editor')
	
	);
	
	$register['testimonials_item'] = array(
		'name' => __('Testimonials','bm-pagebuilder'),
		'meta' => true,
		'add_new' => __('Add New','bm-pagebuilder'),
		'add_new_item' => __('Add New Testimonial','bm-pagebuilder'),
		'edit_item' => __('Edit Testimonial','bm-pagebuilder'),
		'new_item' => __('New Testimonial','bm-pagebuilder'),
		'view_item' => __('View Testimonial','bm-pagebuilder'),
		'not_found' => __('No Testimonial found.','bm-pagebuilder'),
		'not_found_in_trash' => __('No Testimonial found in Trash.','bm-pagebuilder'),
		'search_items' => __('Search Testimonial','bm-pagebuilder'),
		'cat_slug' => __('testimonial_cat','bm-pagebuilder'),
		'cat_menu_name' => __('Categories','bm-pagebuilder'),
		'columns' => array(
			'column_category' => __('Categories','bm-pagebuilder')
		),
		'menu_icon' => UX_PAGEBUILDER. '/images/icon/post-type/testimonial.png'
	
	);
	
	$register['jobs_item'] = array(
		'name' => __('Jobs','bm-pagebuilder'),
		'meta' => true,
		'add_new' => __('Add New','bm-pagebuilder'),
		'add_new_item' => __('Add New Job','bm-pagebuilder'),
		'edit_item' => __('Edit Job','bm-pagebuilder'),
		'new_item' => __('New Job','bm-pagebuilder'),
		'view_item' => __('View Job','bm-pagebuilder'),
		'not_found' => __('No Job found.','bm-pagebuilder'),
		'not_found_in_trash' => __('No Job found in Trash.','bm-pagebuilder'),
		'search_items' => __('Search Job','bm-pagebuilder'),
		'cat_slug' => __('job_cat','bm-pagebuilder'),
		'cat_menu_name' => __('Job Categories','bm-pagebuilder'),
		'columns' => array(
			'column_category' => __('Categories','bm-pagebuilder')
		),
		'menu_icon' => UX_PAGEBUILDER. '/images/icon/post-type/jobs.png'
	
	);
	
	$register['faqs_item'] = array(
		'name' => __('FAQs','bm-pagebuilder'),
		'meta' => false,
		'add_new' => __('Add New','bm-pagebuilder'),
		'add_new_item' => __('Add New Question','bm-pagebuilder'),
		'edit_item' => __('Edit Question','bm-pagebuilder'),
		'new_item' => __('New Question','bm-pagebuilder'),
		'view_item' => __('View Question','bm-pagebuilder'),
		'not_found' => __('No Question found.','bm-pagebuilder'),
		'not_found_in_trash' => __('No Question found in Trash.','bm-pagebuilder'),
		'search_items' => __('Search Question','bm-pagebuilder'),
		'cat_slug' => __('question_cat','bm-pagebuilder'),
		'cat_menu_name' => __('Topics','bm-pagebuilder'),
		'columns' => array(
			'column_category' => __('Categories','bm-pagebuilder')
		),
		'menu_icon' => UX_PAGEBUILDER.'/images/icon/post-type/faqs.png'
	);
	
	return $register;
}
add_filter('ux_theme_register_post_type', 'ux_theme_register_other_post_type');

// Register a menu page.
function ux_theme_register_pb_menu() {
    add_menu_page(
        __( 'BM PageBuilder', 'bm-pagebuilder' ),
        __( 'BM PageBuilder', 'bm-pagebuilder' ),
        'manage_options',
        'bm-pagebuilder',
        'ux_pb_interface_plugin_option'
    );
	add_submenu_page( 'bm-pagebuilder', __( 'Settings', 'bm-pagebuilder' ), __( 'Settings', 'bm-pagebuilder' ), 'manage_options', 'bm-pagebuilder', 'ux_pb_interface_plugin_option');
}
add_action( 'admin_menu', 'ux_theme_register_pb_menu' );
?>