<?php
//pagebuilder module fields
function ux_pb_module_fields(){
	$module_fields = array();
	$module_fields = apply_filters('ux_pb_module_fields', $module_fields);
	$module_fields = ux_pb_animation($module_fields);
	return $module_fields;
}

//pagebuilder config social networks
function ux_pb_social_networks(){
	$ux_pb_social_networks = array(
		array('name' => __('Facebook','bm-pagebuilder'), 'icon' => 'fa fa-facebook-square', 'icon2' => 'fa fa-facebook-square',
			  'slug' => 'facebook', 'dec'  => __('Visit Facebook page','bm-pagebuilder')),
		array('name' => __('Twitter','bm-pagebuilder'), 'icon' => 'fa fa-twitter-square', 'icon2' => 'fa fa-twitter-square',
			  'slug' => 'twitter', 'dec'  => __('Visit Twitter page','bm-pagebuilder')),
		array('name' => __('Google+','bm-pagebuilder'), 'icon' => 'fa fa-google-plus-square', 'icon2' => 'fa fa-google-plus-square',
			  'slug' => 'googleplus', 'dec'  => __('Visit Google Plus page','bm-pagebuilder')),
		array('name' => __('Youtube','bm-pagebuilder'), 'icon' => 'fa fa-youtube-square', 'icon2' => 'fa fa-youtube-square',
			  'slug' => 'youtube', 'dec'  => __('Visit Youtube page','bm-pagebuilder')),
		array('name' => __('Vimeo','bm-pagebuilder'), 'icon' => 'fa fa-vimeo-square', 'icon2' => 'fa fa-vimeo-square',
			  'slug' => 'vimeo', 'dec'  => __('Visit Vimeo page','bm-pagebuilder')),
		array('name' => __('Tumblr','bm-pagebuilder'), 'icon' => 'fa fa-tumblr-square', 'icon2' => 'fa fa-tumblr-square',
			  'slug' => 'tumblr', 'dec'  => __('Visit Tumblr page','bm-pagebuilder')),
		array('name' => __('RSS','bm-pagebuilder'), 'icon' => 'fa fa-rss-square', 'icon2' => 'fa fa-rss-square',
			  'slug' => 'rss', 'dec'  => __('Visit Rss','bm-pagebuilder')),
		array('name' => __('Pinterest','bm-pagebuilder'), 'icon' => 'fa fa-pinterest-square', 'icon2' => 'fa fa-pinterest-square',
			  'slug' => 'pinterest', 'dec'  => __('Visit Pinterest page','bm-pagebuilder')),
		array('name' => __('Linkedin','bm-pagebuilder'), 'icon' => 'fa fa-linkedin-square', 'icon2' => 'fa fa-linkedin-square',
			  'slug' => 'linkedin', 'dec'  => __('Visit Linkedin page','bm-pagebuilder')),
		array('name' => __('Instagram','bm-pagebuilder'), 'icon' => 'fa fa-instagram', 'icon2' => 'fa fa-instagram',
			  'slug' => 'instagram', 'dec'  => __('Visit Instagram page','bm-pagebuilder')),
		array('name' => __('Github','bm-pagebuilder'), 'icon' => 'fa fa-github-square', 'icon2' => 'fa fa-github-square',
			  'slug' => 'github', 'dec'  => __('Visit Github page','bm-pagebuilder')),
		array('name' => __('Xing','bm-pagebuilder'), 'icon' => 'fa fa-xing-square', 'icon2' => 'fa fa-xing-square',
			  'slug' => 'xing', 'dec'  => __('Visit Xing page','bm-pagebuilder')),
		array('name' => __('Flickr','bm-pagebuilder'), 'icon' => 'fa fa-flickr', 'icon2' => 'fa fa-flickr',
			  'slug' => 'flickr', 'dec'  => __('Visit Flickr page','bm-pagebuilder')),
		array('name' => __('VK','bm-pagebuilder'), 'icon' => 'fa fa-vk square-radiu', 'icon2' => 'fa fa-vk square-radiu',
			  'slug' => 'vk', 'dec'  => __('Visit VK page','bm-pagebuilder')),
		array('name' => __('Weibo','bm-pagebuilder'), 'icon' => 'fa fa-weibo square-radiu', 'icon2' => 'fa fa-weibo square-radiu',
			  'slug' => 'weibo', 'dec'  => __('Visit Weibo page','bm-pagebuilder')),
		array('name' => __('Renren','bm-pagebuilder'), 'icon' => 'fa fa-renren square-radiu', 'icon2' => 'fa fa-renren square-radiu',
			  'slug' => 'renren', 'dec'  => __('Visit Renren page','bm-pagebuilder')),
		array('name' => __('Bitbucket','bm-pagebuilder'), 'icon' => 'fa fa-bitbucket-square', 'icon2' => 'fa fa-bitbucket-square',
			  'slug' => 'bitbucket', 'dec'  => __('Visit Bitbucket page','bm-pagebuilder')),
		array('name' => __('Foursquare','bm-pagebuilder'), 'icon' => 'fa fa-foursquare square-radiu', 'icon2' => 'fa fa-foursquare square-radiu',
			  'slug' => 'foursquare', 'dec'  => __('Visit Foursquare page','bm-pagebuilder')),
		array('name' => __('Skype','bm-pagebuilder'), 'icon' => 'fa fa-skype square-radiu', 'icon2' => 'fa fa-skype square-radiu',
			  'slug' => 'skype', 'dec'  => __('Skype','bm-pagebuilder')),
		array('name' => __('Dribbble','bm-pagebuilder'), 'icon' => 'fa fa-dribbble square-radiu', 'icon2' => 'fa fa-dribbble square-radiu',
			  'slug' => 'dribbble', 'dec'  => __('Visit Dribbble page','bm-pagebuilder'))
	);	
	
	return $ux_pb_social_networks;
	
}

//pagebuilder module select fields
function ux_pb_module_select_fields(){
	$module_fields['module_select_orderby'] = array(
		array('title' => __('Please Select','bm-pagebuilder'), 'value' => 'none'),
		array('title' => __('Title','bm-pagebuilder'), 'value' => 'title'),
		array('title' => __('Date','bm-pagebuilder'), 'value' => 'date'),
		array('title' => __('ID','bm-pagebuilder'), 'value' => 'id'),
		array('title' => __('Modified','bm-pagebuilder'), 'value' => 'modified'),
		array('title' => __('Author','bm-pagebuilder'), 'value' => 'author'),
		array('title' => __('Comment count','bm-pagebuilder'), 'value' => 'comment_count')
	);
	
	$module_fields['module_select_order'] = array(
		array('title' => __('Ascending','bm-pagebuilder'), 'value' => 'ASC'),
		array('title' => __('Descending','bm-pagebuilder'), 'value' => 'DESC')
	);
	
	$module_fields['module_bottom_margin'] = array(
		array('title' => __('No Margin','bm-pagebuilder'), 'value' => 'bottom-space-no'),
		array('title' => __('20px','bm-pagebuilder'), 'value' => 'bottom-space-20'),
		array('title' => __('40px','bm-pagebuilder'), 'value' => 'bottom-space-40'),
		array('title' => __('60px','bm-pagebuilder'), 'value' => 'bottom-space-60'),
		array('title' => __('80px','bm-pagebuilder'), 'value' => 'bottom-space-80')
	);
	
	$module_fields['module_scroll_animation_one'] = array(
		array('title' => __('Fade in','bm-pagebuilder'), 'value' => 'fadein'),
		array('title' => __('Fade in and zoom in','bm-pagebuilder'), 'value' => 'zoomin'),
		array('title' => __('Fade in from left','bm-pagebuilder'), 'value' => 'from-left-translate'),
		array('title' => __('Fade in from right','bm-pagebuilder'), 'value' => 'from-right-translate'),
		array('title' => __('Fade in from top','bm-pagebuilder'), 'value' => 'from-top-translate'),
		array('title' => __('Fade in from bottom','bm-pagebuilder'), 'value' => 'from-bottom-translate')
	);
	
	$module_fields['module_scroll_animation_two'] = array(
		array('title' => __('Fade in','bm-pagebuilder'), 'value' => 'fadein')
		//array('title' => __('Fade in and zoom in','bm-pagebuilder'), 'value' => 'zoomin'),
		//array('title' => __('Fade in from bottom','bm-pagebuilder'), 'value' => 'from-bottom-translate')
	);
	
	$module_fields['module_scroll_animation_three'] = array(
		array('title' => __('Fade In','bm-pagebuilder'), 'value' => 'fadein'),
		array('title' => __('Fade In Left','bm-pagebuilder'), 'value' => 'from-left-translate'),
		array('title' => __('Fade In Right','bm-pagebuilder'), 'value' => 'from-right-translate'),
		array('title' => __('Fade In Up','bm-pagebuilder'), 'value' => 'from-top-translate'),
		array('title' => __('Fade In Down','bm-pagebuilder'), 'value' => 'from-bottom-translate'),
		array('title' => __('Bounce In Left','bm-pagebuilder'), 'value' => 'bouncdein-left-translate'),
		array('title' => __('Bounce In Right','bm-pagebuilder'), 'value' => 'bouncdein-right-translate'),
		array('title' => __('Bounce In Up','bm-pagebuilder'), 'value' => 'bouncdein-up-translate'),
		array('title' => __('Bounce In Down','bm-pagebuilder'), 'value' => 'bouncdein-down-translate'),
		array('title' => __('Flip X','bm-pagebuilder'), 'value' => 'flip-x-translate'),
		array('title' => __('Flip Y','bm-pagebuilder'), 'value' => 'flip-y-translate'),
		array('title' => __('Rotate In DownLeft','bm-pagebuilder'), 'value' => 'rotate-downleft-translate'),
		array('title' => __('Rotate In DownRight','bm-pagebuilder'), 'value' => 'rotate-downright-translate')
	);
	
	$module_fields = apply_filters('ux_pb_module_select_fields', $module_fields);
	return $module_fields;
}
?>