<?php
/**
 * Plugin Name: Cowidgets - Elementor Addons
 * Plugin URI:  https://cowidgets.com
 * Description: Elementor Widgets for Sliders, Portfolio, Posts, Header & Footer Builder for your WordPress website using Elementor Page Builder for free.
 * Author:      Codeless
 * Author URI:  https://codeless.co
 * Text Domain: cowidgets
 * Domain Path: /languages
 * Version: 1.0.10
 *
 * @package         cowidgets
 */

define( 'COWIDGETS_VER', '1.0.10' );
define( 'COWIDGETS_DIR', plugin_dir_path( __FILE__ ) );
define( 'COWIDGETS_URL', plugins_url( '/', __FILE__ ) );
define( 'COWIDGETS_PATH', plugin_basename( __FILE__ ) );

/**
 * Load the class loader.
 */
require_once COWIDGETS_DIR . '/inc/class-cowidgets.php';

/**
 * Load the Plugin Class.
 */
function cowidgets_init() {
	CoWidgets::instance();
}

add_action( 'plugins_loaded', 'COWIDGETS_init' );
