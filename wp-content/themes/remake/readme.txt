# Remake The WordPress 

## Description

Remake a minimal and modern brutalist design for Portfolio & Creative Agency websites

## Changelog

### 1.0.6

Fixed: Issue on Elementor Edit page when preloader animation active

### 1.0.5

Fixed: Small theme-check issue

### 1.0.4

Fixed: Elementor loading issue on editor.
Fixed: Admin-bar Menu Issue

### 1.0.3

Added: Two new demos: Vjosa Boxed & Vjosa Fullscreen specially for video slider
Fixed: Link on Ishmi style fixed
Added: Orderby options on Sliders query selector.
Added: Video support on Kiri and Hudson slider
Added: New Slider (specifically for videos) Vjosa

### 1.0.2

Fixed: Remove WooCommerce wizard redirection on demo installation
Fixed: Warnings notices when importing
Fixed: JS error on some hosts.
Fixed: Active Elementor on Portfolio items by default
Added: Portfolio Slug option
Fixed: Back-to-top button not working
Fixed: Small CSS & Options issues on Cowidgets plugin

### 1.0.1

Important Update: Deactivate/Delete Cowidgets plugin and Install the new Cowidget plugin recommended by theme hosted on WordPress.org

### 1.0

* Released: September, 2020

Initial release