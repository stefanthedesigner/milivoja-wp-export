<?php
define('UX_WOOCOMMERCE', get_template_directory_uri(). '/woocommerce');

// Theme Get Woo Template
function arnold_get_woo_template_part($key, $name){
	get_template_part('woocommerce/arnold-template/' . $key, $name);
}

//require theme woocommerce register
require_once get_template_directory() . '/woocommerce/arnold-woocommerce-register.php';

//require theme woocommerce functions
require_once get_template_directory() . '/woocommerce/arnold-woocommerce-functions.php';

//require theme woocommerce hook
require_once get_template_directory() . '/woocommerce/arnold-woocommerce-hook.php';

?>