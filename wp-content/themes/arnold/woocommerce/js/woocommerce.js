jQuery(document).ready(function($) {
	
	//product tabs
	jQuery('.nav-tabs a').click(function (e) {
		e.preventDefault();
		jQuery(this).tab('show');
		return false;
	}); 

	//
	if(jQuery('.product-wrap').find('.flexslider').length){
		
		jQuery('.product-wrap').find('.flexslider').flexslider({
				animation: 'slide', //String: Select your animation type, "fade" or "slide"
				animationLoop: true,
				slideshow: true, 
				smoothHeight: true,  
				controlNav: true, //Dot Nav
				directionNav: false,  // Next\Prev Nav
				touch: true, 
				slideshowSpeed: 3000
			});
	
	}
	
	//show shipping calculator box in Cart page
	if(jQuery('.shipping-calculator-form').length){
		
		jQuery('.shipping-calculator-form').siblings('h4.lined-heading').css('cursor','pointer');
		
		jQuery('.shipping-calculator-form').siblings('h4.lined-heading').click(function(){
		
			jQuery(".shipping-calculator-form").slideToggle(500, function() {
				if (jQuery(this).is(":visible")) {
					
				}
			});

			return false;
		
		})
		
	}
	
	//Close Login form box
	jQuery('.modal-header').find('button').click(function(){
	
		if(jQuery('#login-form,#modal-mask').hasClass('in')){
		
			jQuery('#login-form,#modal-mask').removeClass('in').addClass('out');
		
		}

		return false;
	
	});
	//open Login formbox
	jQuery('.show-login').click(function(){
	
		if(!jQuery('#login-form,#modal-mask').hasClass('in')){
		
			jQuery('#login-form,#modal-mask').addClass('in').removeClass('out');
			jQuery('#login-form').find('form').show(300);
		
		}

		return false;
	
	});

	jQuery('.woocommerce-product-rating').each(function() { 
		jQuery(this).appendTo('.price');

	});
	
	// if(jQuery('#payment .payment_methods').length){
	// 	jQuery('.payment_methods > li').each(function(){
	// 		jQuery(this).find('input.input-radio').click('click', function(){
	// 			console.log('s');
	// 		});
	// 	});
	// }

	if(jQuery('.gallery-shopping-product').length && jQuery('.quantity').length){
		var _text = jQuery('.gallery-shopping-product').data('label');
		jQuery('.quantity .qty').before('<span class="quantity-label">' + _text + '</span>');
	}
});

jQuery('.woocommerce-message').find('.button').removeClass('button').addClass('ux-btn');