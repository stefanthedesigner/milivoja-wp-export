<?php
//woocommerce register sidebar
function arnold_woocommerce_register_sidebar(){
	register_sidebar(array(
		'name' => __('Shop Sidebar', 'arnold'),
		'id' => 'ux-shop-sidebar',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'class' => ''
	));
}
add_action('widgets_init', 'arnold_woocommerce_register_sidebar');

//woocommerce enqueue script
function arnold_woocommerce_enqueue_scripts(){
	wp_register_script('flexslider', UX_WOOCOMMERCE. '/js/jquery.flexslider-min.js', array('jquery'), '2.6.3', true);
	wp_register_script('arnold-woocommerce', UX_WOOCOMMERCE. '/js/woocommerce.js', array('jquery'), '0.0.1', true);
	wp_register_style('arnold-woocommerce', UX_WOOCOMMERCE. '/css/woocommerce.css', array(), '0.0.1', 'screen');
		wp_dequeue_style('woocommerce_frontend_styles');
		wp_enqueue_script('flexslider');
		wp_enqueue_script('arnold-woocommerce');
		wp_enqueue_style('arnold-woocommerce');
}
add_action('wp_enqueue_scripts', 'arnold_woocommerce_enqueue_scripts', 100);

?>