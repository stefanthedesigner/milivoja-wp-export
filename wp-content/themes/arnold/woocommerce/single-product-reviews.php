<?php
/**
 * Display single product reviews (comments)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product-reviews.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! comments_open() ) {
	return;
}

global $woocommerce, $product;
if(comments_open()){
	$get_comments = get_comments(array(
		'post_id' => get_the_ID(),
		'status' => 'approve'
	)); ?>
	<div id="comments">
		<div id="comments_box">
			<span id="comments_inlist">
				<?php
				$count = $product->get_review_count();
				if ( $count && wc_review_ratings_enabled() ) {
					/* translators: 1: reviews count 2: product name */
					$reviews_title = sprintf( esc_html( _n( '%1$s review for %2$s', '%1$s reviews for %2$s', $count, 'woocommerce' ) ), esc_html( $count ), '<span>' . get_the_title() . '</span>' );
					echo apply_filters( 'woocommerce_reviews_title', $reviews_title, $count, $product ); // WPCS: XSS ok.
				} else {
					esc_html_e( 'Reviews', 'woocommerce' );
				}
				?>
			</span>
			<?php if($get_comments){ ?>
				<ol class="commentlist commentlist-only">
					<?php foreach($get_comments as $comment){
						$rating = esc_attr(get_comment_meta($comment->comment_ID, 'rating', true)); ?>
						<li id="comment-<?php echo esc_attr($comment->comment_ID); ?>" class="commlist-unit">
							<div class="comm-u-wrap">
								<div class="comment">
									<p><?php echo esc_attr($comment->comment_content); ?></p>
								</div><!--END comment-->
								<div class="comment-meta">
									<span class="comment-author"><a href="<?php comment_author_url($comment->comment_ID); ?>"><?php comment_author($comment->comment_ID); ?></a></span>
									<span class="date"><?php echo human_time_diff(get_comment_date('U', $comment->comment_ID), current_time('timestamp'));  esc_html_e(" ago",'arnold'); ?></span>
									<?php if ( wc_review_ratings_enabled() ) { ?>
									<span class="rating_container"> 
									<div class="star-rating" title="<?php echo sprintf(esc_html__( 'Rated %d out of 5', 'arnold' ), $rating) ?>"><span style="width:<?php echo (intval(get_comment_meta($comment->comment_ID, 'rating', true ) ) / 5 ) * 100; ?>%"><strong itemprop="ratingValue" class="rating"><?php echo intval(get_comment_meta($comment->comment_ID, 'rating', true)); ?></strong> <?php _e( 'out of 5', 'arnold' ); ?></span></div>
									</span>
									<?php } ?>
								</div><!--END comment-mate--> 
								<span class="reply"></span>		
							</div><!--END comm-u-wrap-->
	  
						</li><!-- #comment-## -->
					<?php } ?>
				</ol>
			<?php } ?>
		</div><!-- #comments_box-->	
	</div>
<?php
}