<?php 
$show_cart = arnold_get_option('theme_option_show_shopping_cart');
$hide_cart_ifempty = arnold_get_option('theme_option_show_shopping_cart_hide_empty');
$hide_cart_class = '';
if ( WC()->cart->get_cart_contents_count() == 0  && $hide_cart_ifempty === true && $show_cart === true ) {
	$hide_cart_class = 'hidden';
} 
?>

<div id="woocomerce-cart-side" class="ux-woo-cart-wrap <?php echo sanitize_html_class($hide_cart_class); ?>">
    <a href="<?php echo esc_url(wc_get_cart_url()); ?>" class="ux-woocomerce-cart-a">
    	<span class="ux-bag"> 
    		<svg version="1.1" id="Layer_1" x="0px" y="0px" width="20px" height="20px" viewBox="-31.5 96.5 20 20" enable-background="new -31.5 96.5 20 20" xml:space="preserve">
				<path d="M-24.5,104v-3.957c0-1.681,1.344-3.043,3-3.043s3,1.362,3,3.043V104" class="ux-bag-path"/>
				<path stroke-miterlimit="10" stroke-width="1" fill="currentColor" d="M-28.5,101.5v15h14v-15H-28.5z M-15.5,115.5h-12v-13h12V115.5z"/>
			</svg>
        	<span class="woocomerce-cart-number"><?php echo sizeof(WC()->cart->get_cart()); ?></span>
    	</span>
    </a>
</div> 