<?php 
$arnold_woo_term_object = get_queried_object(); 
$arnold_woo_cate_des = $arnold_woo_term_object->description; 
if ( ! $arnold_woo_cate_des ) {
	$arnold_woo_cate_des = $wp_query->found_posts . esc_html__(' items found','arnold'); 
}
if ( is_shop() ) { 
	$arnold_woo_cate_des = arnold_get_option('theme_option_descriptions_woo_shop_subtit');
}
?>
<div class="archive-title title-wrap">
    <div class="title-wrap-con">
          <h2 class="title-wrap-tit"><?php woocommerce_page_title(); ?></h2>
          <div class="archive-des"><?php echo esc_html( $arnold_woo_cate_des ); ?></div>
    </div>
</div>