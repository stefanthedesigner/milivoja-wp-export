<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.5.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $woocommerce, $product;
$thumbnail_size    = apply_filters( 'woocommerce_product_thumbnails_large_size', 'full' );
$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
$full_size_image   = wp_get_attachment_image_src( $post_thumbnail_id, $thumbnail_size );
$attachment_ids = $product->get_gallery_image_ids(); ?>

<div class="images flex-slider-wrap col-md-6 col-sm-6">

    <?php woocommerce_show_product_sale_flash(); ?>
    
    <?php $flexslider = $attachment_ids ? 'flexslider' : false; 
    $attributes = array(
        'title'                   => get_post_field( 'post_title', $post_thumbnail_id ),
        'data-caption'            => get_post_field( 'post_excerpt', $post_thumbnail_id ),
        'data-src'                => $full_size_image[0],
        'data-large_image'        => $full_size_image[0],
        'data-large_image_width'  => $full_size_image[1],
        'data-large_image_height' => $full_size_image[2],
        'class' => 'product-slider-image',
    );
    ?>
    
    <div id="product-img-slider" class="<?php echo esc_attr($flexslider); ?>">
        <div class="ux-lightbox-wrap lightbox-photoswipe">
            <span class="fa fa-clone"></span>
            <?php 

            if($product->get_image_id()){ ?>
                <div class="lightbox-wrap-item" data-lightbox="true">
                    <a href="<?php echo esc_url( $full_size_image[0] ); ?>" class="lightbox-wrap-triggle lightbox-item" data-size="<?php echo esc_attr($full_size_image[1]); ?>x<?php echo esc_attr($full_size_image[2]); ?>">
                        <?php echo get_the_post_thumbnail( $post->ID, 'medium', $attributes ); ?>
                    </a>
                </div>
            <?php
            }
            if ( $attachment_ids ) {
                foreach ( $attachment_ids as $attachment_id ) {
                    $full_size_image = wp_get_attachment_image_src( $attachment_id, 'full' );
                    $attr = array(
                        'title'                   => get_post_field( 'post_title', $attachment_id ),
                        'data-caption'            => get_post_field( 'post_excerpt', $attachment_id ),
                        'data-src'                => $full_size_image[0],
                        'data-large_image'        => $full_size_image[0],
                        'data-large_image_width'  => $full_size_image[1],
                        'data-large_image_height' => $full_size_image[2],
                        'class' => 'hidden',
                    ); ?>
                    <div class="lightbox-wrap-item" data-lightbox="true">
                        <a href="<?php echo esc_url( $full_size_image[0] ); ?>" class="lightbox-wrap-triggle lightbox-item" data-size="<?php echo esc_attr($full_size_image[1]); ?>x<?php echo esc_attr($full_size_image[2]); ?>">
                    <?php 
                    echo wp_get_attachment_image( $attachment_id, 'medium', false, $attr ); ?>
                    </a></div>
                    <?php
                }
            }
            ?>
            
        </div>
    
        <ul class="slides">
			<?php 
                if ( $product->get_image_id() ) {
                $html  = '<li data-thumb="' . get_the_post_thumbnail_url( $post->ID, 'arnold-standard-thumb-medium' ) . '" class="woocommerce-product-gallery__image">';
                $html .= get_the_post_thumbnail( $post->ID, 'shop_single', $attributes );
                $html .= '</li>';
            } else {
                $html  = '<li class="woocommerce-product-gallery__image--placeholder">';
                $html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src() ), esc_html__( 'Awaiting product image', 'arnold' ) );
                $html .= '</li>';
            }

            echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, get_post_thumbnail_id( $post->ID ) );

            do_action( 'woocommerce_product_thumbnails' );
            ?>
        </ul>
        
    </div>
    
</div>


