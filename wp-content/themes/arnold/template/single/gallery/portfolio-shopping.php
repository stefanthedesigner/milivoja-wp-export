<?php
$shopping_select_product = arnold_get_post_meta(get_the_ID(), 'theme_meta_shopping_select_product');
$number_text = arnold_get_option('theme_option_descriptions_woo_quantity_shop_box') ? arnold_get_option('theme_option_descriptions_woo_quantity_shop_box') : esc_attr__('Amount', 'arnold');

if($shopping_select_product){
	$get_post = get_post($shopping_select_product);
	
	if(function_exists('wc_get_product') && $get_post){
		$GLOBALS['product'] = wc_get_product($get_post);
		global $product; ?>
		
		<div class="gallery-shopping-product" data-label="<?php echo esc_attr($number_text); ?>">
            <div class="gallery-shopping-product-price">
				<?php echo wp_kses($product->get_price_html(), arnold_shapeSpace_allowed_html()); ?>
            </div>
            
            <div class="gallery-shopping-product-box">
				<div class="gallery-shopping-product-box-close"></div>
				<?php if(function_exists('woocommerce_template_single_add_to_cart')){
                    woocommerce_template_single_add_to_cart();
                } ?>
            </div>
            
        </div>
	<?php
    }
}

?>