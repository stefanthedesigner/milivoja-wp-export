<?php
//template
$gallery_template = arnold_get_post_meta(get_the_ID(), 'theme_meta_gallery_template');

//width
$gallery_width = arnold_get_post_meta(get_the_ID(), 'theme_meta_gallery_width');

//spacing
$gallery_image_spacing = arnold_get_post_meta(get_the_ID(), 'theme_meta_gallery_image_spacing');
$gallery_spacing_class = '';
if($gallery_image_spacing){
	$gallery_spacing_class = 'gallery-spacing-' .$gallery_image_spacing;
}

$gallery_col_class = '';
$gallery_width_class = '';
if($gallery_template == 'standard' || $gallery_template == 'big-title'){
	switch($gallery_width){
		case 'normal' : $gallery_width_class = 'container'; break;
	}
}
?>

<div class="<?php echo sanitize_html_class($gallery_col_class); ?> <?php echo sanitize_html_class($gallery_spacing_class); ?>  blog-unit-gallery-wrap <?php echo esc_attr($gallery_width_class); ?>">

    <div class="single-gallery-wrap-inn">

		<?php
		$gallery_video_position = arnold_get_post_meta(get_the_ID(), 'theme_meta_gallery_video_position');
		
		if($gallery_video_position == 'top'){
			//Video
			arnold_get_template_part('single/gallery/portfolio', 'video');
		}
		
        //** get portfolio image
        $portfolio = arnold_get_post_meta(get_the_ID(), 'theme_meta_portfolio');
		if ( $portfolio ) {
			if ( ! is_array( $portfolio ) ) { $portfolio = array( $portfolio ); }
		}
		
		//** get layout builder content
		$layout_builder_content = arnold_get_post_meta(get_the_ID(), 'layout-builder-content');
		
        //** get portfolio list layout builder
        $layout_builder = arnold_get_post_meta(get_the_ID(), 'theme_meta_enable_portfolio_list_layout_builder');
		
		$layouts = array();
		
		$portfolioIndex = 0;
		$contentIndex = 0;
		
		if ( $layout_builder ) {
			$layoutEnd = end( $layout_builder );
			$portfolioCount = count( $portfolio );
			echo '<div class="list-layout lightbox-photoswipe" data-gap="' .esc_attr( $gallery_image_spacing ). '">';
			foreach ( $layout_builder as $num => $layout ) {
				switch ( $layout ) {
					case 'list_layout_1': $i = 1; $layoutClass = 'list-layout-col1'; break;
					case 'list_layout_2': $i = 2; $layoutClass = 'list-layout-col2'; break;
					case 'list_layout_3': $i = 3; $layoutClass = 'list-layout-col3'; break;
					case 'list_layout_4': $i = 4; $layoutClass = 'list-layout-col4'; break;
					case 'list_layout_5': $i = 1; $layoutClass = 'list-layout-col1'; break;
				}
				
				echo '<div class="list-layout-col ' .sanitize_html_class($layoutClass). ' clearfix">';
				for ( $ii=0; $ii<$i; $ii++ ) {
					if ( $layout == 'list_layout_5' ) {
						arnold_interface_portfolio_template_layout( 0, 'list_layout_5', $contentIndex );
					} else {
						if ( $portfolio ) {
							if ( isset( $portfolio[$portfolioIndex] ) ) {
								arnold_interface_portfolio_template_layout( $portfolio[$portfolioIndex], $layout, $portfolioIndex );
								$portfolioIndex++;
							}
						}
					}
				}
				echo '</div>';
				$contentIndex++;
			}
			
			if ( $portfolioIndex < $portfolioCount ) {
				$remaining = $portfolioCount - $portfolioIndex;
				if($remaining > 0){
					switch ( $layoutEnd ) {
						case 'list_layout_1': $i = 1; $layoutClass = 'list-layout-col1'; break;
						case 'list_layout_2': $i = 2; $layoutClass = 'list-layout-col2'; break;
						case 'list_layout_3': $i = 3; $layoutClass = 'list-layout-col3'; break;
						case 'list_layout_4': $i = 4; $layoutClass = 'list-layout-col4'; break;
						case 'list_layout_5': $i = 1; $layoutClass = 'list-layout-col1'; break;
					}
					
					$row = ceil( $remaining / $i );
					$thisII = 0;
					/*if ( ( count( $layout_builder ) == 1 ) && ( $layoutEnd == 'list_layout_5' ) ) {
						$thisII = $row;
					}*/
					
					for ( $ii=$thisII; $ii<$row; $ii++ ) { ?>
                        <div class="list-layout-col <?php echo sanitize_html_class( $layoutClass ); ?> clearfix">
                            <?php for ( $iii=0; $iii<$i; $iii++ ) {
                                if ( isset( $portfolio[$portfolioIndex] ) ) {
                                    if ( $layoutEnd == 'list_layout_5') { $layoutEnd = 'list_layout_1'; }
									arnold_interface_portfolio_template_layout( $portfolio[$portfolioIndex], $layoutEnd );
									$portfolioIndex++;
                                }
                            } ?>
                        </div>
                    <?php	
                    }
				}
			}
			echo '</div>';
		}
		
		if($gallery_video_position == 'bottom'){
			//Video
			arnold_get_template_part('single/gallery/portfolio', 'video');
		} ?>
    
    </div>
</div>