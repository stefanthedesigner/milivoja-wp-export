<?php
define( 'WP_CACHE', true ); // Added by WP Rocket


//Begin Really Simple SSL session cookie settings
@ini_set('session.cookie_httponly', true);
@ini_set('session.cookie_secure', true);
@ini_set('session.use_only_cookies', true);
//END Really Simple SSL

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpressuser' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password' );

define('FS_METHOD', 'direct');

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '&CBUMgFy-l<~v&t7*WR,K0Av$#,(8xp]^CA-medD+schxH@JYPU|a HFOc-,-m1}' );
define( 'SECURE_AUTH_KEY',  '15Y7+IiNN5^-o3Z;e1-}3_3fwAN+%|(j</U#/,qJ{C m[9KK,Bz~$v?2R/ZBitFp' );
define( 'LOGGED_IN_KEY',    'vI +(`LoR=m.(Kx4Y !|=,D,P$z[[G1pG#G(:p{|7h8#-5gSu(RJ#wcx?i&oh;E:' );
define( 'NONCE_KEY',        '?YYVA`Fct1Uo+{!.bZe-VXF{?K5x$v=pLk@2Zxw>;|hdR1|LM.-Z#z/v5dM8=3a%' );
define( 'AUTH_SALT',        '@F~- mS|i<b$6!gGqi*|$jib7}?GlYz67Y}d[}/*IW-no(0%n}xdGzq$7WW{-HwC' );
define( 'SECURE_AUTH_SALT', 'hr-)js-h>$z[?bMF7P]E=}oE)qK|!l+9E#M[_nb^(q) 1^(ZKtTj~IJ0be!sI:3q' );
define( 'LOGGED_IN_SALT',   'u!b;Prl=e- 6z-DE`;+yW9z;%X>&b*:ii;tH$7-aT,tyoL)In]*8W6@Eh(D+;8_0' );
define( 'NONCE_SALT',       'H?|r*&6e-q4hW+*L1!SvqA&AK-D=eQrjdRXc>#I<m2akh!<L|kU>At(0Vf>:f|F_' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

@ini_set('upload_max_size', '1000M');

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
